import numpy as np


def split(x, y):
    """ Let f(x) = c1 if x < x0 else c2
        Find such x0, c1, c2, that minimize RMSE(y, f(x)).

        RMSE = sqrt(sum((y[i] - y_hat[i]) ** 2) / len(y))
        Equivalently, you can minimize "squared residuals sum"
        SRS = sum((y[i] - y_hat[i]) ** 2)
    """
    assert len(x) and len(x) == len(y)
    ind = np.argsort(x)
    x, y = np.array(x)[ind], np.array(y)[ind]

    n = len(y)
    s1, s2 = 0, sum(y)
    q1, q2 = 0, sum(yi * yi for yi in y)
    best_srs, best_n = np.inf, None

    for n1 in range(1, n):
        yi = y[n1 - 1]
        s1, s2 = s1 + yi, s2 - yi
        sq = yi * yi
        q1, q2 = q1 + sq, q2 - sq

        # in our case srs = srs1 + srs2. you can simplify each of them to:
        # sum((y[i] - y_mean) ** 2) =
        # sum(y[i]**2) - 2*sum(y)*y_mean + len(y)*y_mean**2 =
        # sum(y[i]**2) - sum(y)**2 / len(y) =
        # q - s * s / n

        srs = (q1 - s1 * s1 / n1) + (q2 - s2 * s2 / (n - n1))

        if srs < best_srs:
            best_srs, best_n = srs, n1

    x0 = (x[best_n - 1] + x[best_n]) * .5
    c1, c2 = y[: best_n].mean(), y[best_n:].mean()
    return x0, c1, c2

